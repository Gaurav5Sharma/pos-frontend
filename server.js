/* eslint-disable no-console */
/**
 * Setup and run the development server for Hot-Module-Replacement
 * https://webpack.github.io/docs/hot-module-replacement-with-webpack.html
 * @flow
 */

import express from 'express';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import { spawn } from 'child_process';
//import dbConfigObj from './app/utils/localDbConfig'
import fs from 'fs';
import bodyParser from 'body-parser';
///console.log(dbConfigObj);

import config from './webpack.config.development';

const argv = require('minimist')(process.argv.slice(2));

const app = express();
app.use(bodyParser.json());
const compiler = webpack(config);
const PORT = process.env.PORT || 3000;

const wdm = webpackDevMiddleware(compiler, {
  publicPath: config.output.publicPath,
  stats: {
    colors: true
  }
});
/*Read write file*/

app.use(wdm);
/*Start API*/
app.get("/contacts", function(req, res) {
  var newData;
  fs.readFile('text.txt', function(err, data){
  newData = JSON.parse(data);
  //newData=[{"Hello":"Test data"}];
  res.send(newData)
   });
});

app.post("/contacts", function(req, res){
  var newData;
  fs.readFile('text.txt', function(err, data){
  console.log(data);
  newData = JSON.parse(data);
  newData.push(req.body);
  fs.writeFile('text.txt',JSON.stringify(newData), function(err){});
  res.send(req.body);
   });
});
/*End Api*/
app.use(webpackHotMiddleware(compiler));

const server = app.listen(PORT, 'localhost', serverError => {
  if (serverError) {
    return console.error(serverError);
  }

  if (argv['start-hot']) {
    spawn('npm', ['run', 'start-hot'], { shell: true, env: process.env, stdio: 'inherit' })
      .on('close', code => process.exit(code))
      .on('error', spawnError => console.error(spawnError));
  }

//  console.log(`Listening at http://localhost:${PORT}`);
});

process.on('SIGTERM', () => {
  console.log('Stopping dev server');
  wdm.close();
  server.close(() => {
    process.exit(0);
  });
});
