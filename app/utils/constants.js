export const GOTAPIDATA = 'GOTAPIDATA';
export const UPDATEAPIDATA = 'UPDATEAPIDATA';
export const GETSEATDATA = 'GETSEATDATA';
export const CHANGETAB = 'CHANGETAB';
export const CONFIRMBOOKING = 'CONFIRMBOOKING';
export const TICKETDATA = 'TICKETDATA';
export const SELECTEDSEATS = 'SELECTEDSEATS';
export const UPDATELAYOUT = 'UPDATELAYOUT';
