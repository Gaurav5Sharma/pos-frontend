// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {Link} from 'react-router';
import styles from './BookTickets.css';

class BookTickets extends Component {
  render() {
    return (
      <div>
        This is Book Tickets Screen.
        <Link to="/layout">Go to Layout Page</Link>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(BookTickets);

