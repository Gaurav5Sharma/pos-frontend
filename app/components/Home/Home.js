// @flow
import React, { Component } from 'react';
import { Link } from 'react-router';
import styles from './Home.css';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as testaction from '../../actions/testaction';
import BookTickets from '../BookTickets/BookTickets'
import HistoricalStatement from '../HistoricalStatement/HistoricalStatement'
import FindBooking from '../FindBooking/FindBooking'
import ConfirmBooking from '../ConfirmBooking/ConfirmBooking'

class Home extends Component {

  test() {
  console.log(`Test button Pressed`);
    this.props.getAPIData();
  }

  updateData() { 
   
    console.log(`Update button Pressed`);
    this.props.updateData();
  }

  render() {
    const {currentTab} = this.props;
    return (
      <div>
        {currentTab === 'BookTickets' && <BookTickets />}
        {currentTab === 'HistoricalStatement' && <HistoricalStatement />}
        {currentTab === 'FindBooking' && <FindBooking />}
        {currentTab === 'ConfirmBooking' && <ConfirmBooking />}
      </div>
    );
  }
}

function mapStateToProps(state) {
  console.log(state);
  return {currentTab: state.currentTab};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(testaction, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);

