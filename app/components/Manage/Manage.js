// @flow
import React, { Component } from 'react';
import { Link } from 'react-router';


export default class Manage extends Component {
  render() {
    return (
      <div>
        <div>
          <h2>This is another page with the url '/manage'</h2>
        </div>

        <Link to="/">Let's go back to home page</Link>
      </div>
    );
  }
}
