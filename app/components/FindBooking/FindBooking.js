// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import './FindBooking.scss';
import fetch from 'fetch';
import {findBookingUrl} from '../../utils/urls'

var _this;

class FindBooking extends Component {

  constructor(props, context) {
    super(props, context);
    _this = this;
    _this.state = {
      invalidBookingId: false,
      bookingId: '',
      bookingDetail: null
    };
  }

  inputChanged(input) {
    _this.setState({bookingId: input, invalidBookingId: false});
  }

  searchBooking() {
    let bookingId = _this.state.bookingId;
    /// TODO -- below condition should be made on the basis of error response.
    if(bookingId.length > 0 && bookingId === '11'){
      _this.setState({invalidBookingId: false}, () => {
        // TODO -- call proper URL below.
        fetch.fetchUrl(findBookingUrl + bookingId, function (error, meta, body) {
          // TODO -- remove below line and add proper response from server to state
          _this.setState({
            bookingDetail: {
              movieName: 'Dear Zindagi',
              movieCertificate: 'U/A',
              movieLanguage: 'Hindi',
              mobileNumber: '+91 7838369367',
              movieDetails: {
                date: {key: 'Date', value: '2nd May'},
                time: {key: 'Time', value: '2:30 PM'},
                ' ': {key: ' ', value: ' '},
                count: {key: 'No. of Tickets', value: '02'},
                audi: {key: 'Audi', value: '03'},
                seats: {key: 'Seats', value: 'H-12; K-13, 14'}
              }
            }
          });
        });
      });
    } else {
      bookingId.length > 0 && _this.setState({invalidBookingId: 'Invalid Booking ID'})
    }
  }

  printTicket() {
    _this.setState({bookingId: '', bookingDetail: null});
  }

  render() {
    const { bookingDetail, invalidBookingId, bookingId } = _this.state;
    return (
      <div className="findBooking">
        <div className="performSearch">
          <div className="title">Enter Booking ID to view details and print the ticket</div>
          <div className="searchBooking">
            <input className={`${invalidBookingId ? 'invalid': ''}`} value={bookingId}
                   placeholder="Booking ID" onChange={(event)=>{_this.inputChanged(event.target.value)}} />
            <button className={`${bookingId && bookingId.length != 0 ? '' : 'disabled'}`} onClick={()=>{_this.searchBooking()}}>Search Booking</button>
          </div>
          {invalidBookingId && <div className="invalidBooking">{invalidBookingId}</div>}
        </div>
        {bookingDetail && <div className="searchedResult">
          <div className="leftSide">
            <div className="title">{bookingDetail.movieName}({bookingDetail.movieCertificate}) | {bookingDetail.movieLanguage}</div>
            <div className="items">
              {Object.keys(bookingDetail.movieDetails).map((item, index)=>{
                return (
                  <div key={index} className="item">
                    <div className="value">{bookingDetail.movieDetails[item].value}</div>
                    <div className="field">{bookingDetail.movieDetails[item].key}</div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className="rightSide">
            <div className="title">Please confirm the below number</div>
            <div className="mobile">{bookingDetail.mobileNumber}</div>
            <button onClick={() => {this.printTicket()}}>Print Ticket</button>
          </div>
        </div>}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(FindBooking);

