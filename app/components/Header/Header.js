// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as headerActions from '../../actions/headerActions';
import { Link } from 'react-router';
import './Header.scss';

class Header extends Component {

  switchTab(tab) {
    this.props.switchTab(tab);
  }

  render() {
    return (
      <div className="header">
        <div className="leftSide">
          <div className="paytmLogo"></div>
          <div className={`tabSelect ${this.props.currentTab === 'BookTickets' ? 'selected' : ''}`} onClick={() => {this.switchTab('BookTickets')}}>Book Tickets</div>
          <div className={`tabSelect ${this.props.currentTab === 'HistoricalStatement' ? 'selected' : ''}`} onClick={() => {this.switchTab('HistoricalStatement')}}>Historical Statement</div>
          <div className={`tabSelect ${this.props.currentTab === 'FindBooking' ? 'selected' : ''}`} onClick={() => {this.switchTab('FindBooking')}}>Find Booking</div>
          <div className={`tabSelect ${this.props.currentTab === 'ConfirmBooking' ? 'selected' : ''}`} onClick={() => {this.switchTab('ConfirmBooking')}}>Confirm Booking</div>
        </div>
        <div className="rightSide">
          <div className="logout">Logout</div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {testreducer: state.testreducer, currentTab: state.currentTab};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(headerActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);

