// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as headerActions from '../../actions/headerActions';
import { Link } from 'react-router';
import './ConfirmBooking.scss';

class ConfirmBooking extends Component {
  handleCountChange(opType){
    console.log(opType);
  }

  render() {
    return (
      <div className="confirmBooking">
        <div className="navigationRow">
          <div className="colLeft">
            <span className="tabs arrow"></span>
            <span className="tabs screenName">Confirm Booking</span>
          </div>
          <div className="colRight">  
            <span className="tabs actionLink">Cancel Booking</span>
          </div>
        </div>
        <div className="bookingDetails">          
            <div className="leftSide">
              <div className="title">Dear Zindagi(U/A) | Hindi</div>
              <div className="items">
                <div className="item">
                  <div className="value">2nd May</div>
                  <div className="field">Date</div>
                </div>
                <div className="item">
                  <div className="value">2:30 PM</div>
                  <div className="field">Time</div>
                </div>
                <div className="item">
                  <div  className="value">H-12; K-13, 14</div>
                  <div className="field">Seats</div>
                </div>
                <div className="item">
                  <div  className="value">02</div>
                  <div className="field">No. of Tickets</div>
                </div>
                <div className="item">
                  <div  className="value">03</div>
                  <div className="field">Audi</div>
                </div>
                <div className="item">
                  <div  className="value">1Hr 30 Mins</div>
                  <div className="field">Duration</div>
                </div>
              </div>
            </div>
            <div className="rightSide">
              <div className="title">Total Amount to be collected</div>
              <div className="amount">Rs. 1350</div>
              <button>Confirm & Print Ticket</button>
            </div>          
        </div>
        <div className="changeCalculator">
          <div className="leftSide">
            <div className="title">Calculate Change</div>
            <div className="subtitle">Select notes given by customer:</div>  
            <div className="items">
              <div className="item">
                <div  className="amount">Rs.10</div>
                <div className="numericSelector commonClass">
                  <span data-value="minus">–</span>
                  <input type="number" name="currency_count" value="0" />
                  <span data-value="plus">+</span>
                </div>
              </div>
              <div className="item">
                <div  className="amount">Rs.100</div>
                <div className="numericSelector commonClass">
                  <span data-value="minus">–</span>
                  <input type="number" name="currency_count" value="0" />
                  <span data-value="plus">+</span>
                </div>
              </div>
              <div className="item">
                <div  className="amount">Rs.1000</div>
                <div className="numericSelector commonClass">
                  <span data-value="minus">–</span>
                  <input type="number" name="currency_count" value="0" />
                  <span data-value="plus">+</span>
                </div>
              </div>
              <div className="item">
                <div  className="amount">Rs.10</div>
                <div className="numericSelector commonClass">
                  <span data-value="minus">–</span>
                  <input type="number" name="currency_count" value="0" />
                  <span data-value="plus">+</span>
                </div>
              </div>
              <div className="item">
                <div  className="amount">Rs.100</div>
                <div className="numericSelector commonClass">
                  <span data-value="minus">–</span>
                  <input type="number" name="currency_count" value="0" />
                  <span data-value="plus">+</span>
                </div>
              </div>
              <div className="item">
                <div  className="amount">Rs.1000</div>
                <div className="numericSelector commonClass">
                  <span data-value="minus" onClick={this.handleCountChange.bind(this,"dec")}>–</span>
                  <input type="number" min="0" name="currency_count" value="0" />
                  <span data-value="plus" onClick={this.handleCountChange.bind(this,"inc")}>+</span>
                </div>
              </div>
            </div>          
          </div>
          <div className="rightSide">
              <div className="title">Total Amount to be Paid Back</div>
              <div className="amount">Rs. 0</div>              
            </div>
        </div>        
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {testreducer: state.testreducer};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(headerActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmBooking);

