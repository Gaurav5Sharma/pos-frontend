// @flow
import React, { Component } from 'react';
import { Link } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as confirmBookingActions from '../../actions/confirmBookingActions';
import './PrintTicket.scss';

class PrintTicket extends Component {

  // getTicketData() {    
  //   console.log(`Geting Booking Data`);
  //   this.props.getBookingData();
  // }

  printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;
       document.body.innerHTML = printContents;
       window.print();
       document.body.innerHTML = originalContents;
  }

  render() {
    const {newProps} = this.props;
    return (
      <div>
        <div id="printableArea" >
           <div className="ticket">
               <img src={'http://amovie.gozha.net/images/icons/stroke.svg'}></img>
               <div className="ticket_position"}>
                   <div className="ticket__indecator styles.indecatorPre">
                     <div className="indecatorText preText">
                       <img className="top" src={'http://amovie.gozha.net/images/icons/stars-light.svg'}></img>
                       Offline ticket
                       <img className="bottom" src={'http://amovie.gozha.net/images/icons/stars-light.svg'}></img>
                     </div>
                   </div>
                   <div className="ticket__inner">
                       <div className=ticketSecondary}>
                           <span className="ticket__item">Ticket number 
                            <strong className="ticket__number">a126bym4</strong>
                           </span>
                           <span className="ticket__item ticket__date">25/10/2013</span>
                           <span className="ticket__item ticket__time">17:45</span>
                           <span className="ticket__item">Cinema: <span className="ticket__cinema">Cineworld</span></span>
                           <span className="ticket__item">Hall: <span className="ticket__hall">Visconti</span></span>
                           <span className="ticket__item ticket__price">price: <strong className="ticket__cost">$60</strong></span>
                       </div>
                       <div className=ticketPrimery}>
                           <span className="ticket__item ticket__itemPrimery ticket__film">Film<br/>
                           <strong className="ticket__movie">The Fifth Estate (2013)</strong></span>
                           <span className="ticket__item ticket__itemPrimery">Sits: 
                           <span className="ticket__place">11F, 12F, 13F</span></span>
                       </div>
                   </div>
                   <div className="ticket__indecator indecatorPost">
                     <div className="indecatorText postText">
                       <img className="top" src={'http://amovie.gozha.net/images/icons/stars-light.svg'}></img>
                       Online ticket
                       <img className="bottom" src={'http://amovie.gozha.net/images/icons/stars-light.svg'}></img>
                     </div>
                   </div>
                 </div>
             </div>
         </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  console.log(state);
  return {ticket: state.ticketData};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(confirmBookingActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PrintTicket);

