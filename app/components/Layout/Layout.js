// @flow
import React, {Component} from 'react';
import {Link} from 'react-router';
import $ from 'jquery';
import {GOTAPIDATA} from '../../utils/constants';
import './Layout.scss';
import _ from 'lodash';
var self;
var tempStore;
export default class Layout extends Component {
  constructor(props, context) {
    super(props, context);
    self = this;
  };

  getLayoutData() {
    this.props.getSeatData();
  }

  findMissingRowAndBuildStandardObj() {
    self.state.layoutObjData.map(function (category, catIndex) {
      category.objRow.map(function (rows, rowIndex) {
        var lstColObj = rows.objSeat[rows.objSeat.length - 1];
        var firstColObj = rows.objSeat[0];
        if (true) {
          rows.objSeat.map(function (cols, colIndex) {
            cols.GridSeatNum = cols.GridSeatNum + 1;
          });
        }
      });
    });
    self.setState({
      layoutObjData: self.state.layoutObjData
    });
  }

  buildSeatLayoutRows() {
    self.findMissingRowAndBuildStandardObj();
    if (this.state.layoutObjData) {
      tempStore = 0;
      this.state.layoutObjData.map(function (category, index) {
        category.objRow = self.buildSeatLayoutRowsData(category.objRow, category.max_rows, function (rindex) {
          return {
            "GridRowId": rindex,
            "PhyRowId": "",
            "objSeat": []
          }
        });
        category.objRow.length > 0 && category.objRow.map(function (rows, rowIndex) {
          rows.objSeat = self.buildSeatLayoutUtility(rows.objSeat, 'GridSeatNum', function (cindex) {
            return {
              "GridSeatNum": cindex + 1,
              "SeatStatus": "0",
              "seatNumber": ""
            }
          });
          rows.objSeat.map(function (cols, colIndex) {
            cols['isSelected'] = false;
          });
        });
      });
      this.setState({
        "layoutObjData": this.state.layoutObjData
      });
    }
  }

  /*Build seatlayout obj utility for boxoffice merchants*/
  buildSeatLayoutRowsData(collection, maxSize, callBack) {
    if (collection.length > 0) {
      for (var i = 1; i < maxSize + 1; i++) {
        var isRow = collection[i - 1] && collection[i - 1].GridRowId == tempStore ? true : false;
        if (isRow) {

        } else {
          collection.splice(i - 1, 0, callBack(tempStore));
        }
        tempStore++
      }
    }
    return collection;
  }

  /*This method is to build column data*/

  buildSeatLayoutUtility(collection, props, callBack) {
    if (collection.length > 0) {
      var getLastRowValue = collection[collection.length - 1][props];
      for (var i = 0; i <= getLastRowValue - 1; i++) {
        var gridId = collection[i] && collection[i][props] ? collection[i][props] : "";
        if (gridId && (i + 1 == collection[i][props])) {
        } else {
          collection.splice(i, 0, callBack(i))
        }
      }
    }
    return collection;
  }
  /*Find duplicate obj*/
  findDuplicateObjIndex(arr, obj, props, propsNew) {
    var itemIndex=-1;
    arr.map(function (item, index) {
      if(item[props]==obj[props] && item[propsNew]==obj[propsNew]){
        itemIndex = index;
      }
    });
    return itemIndex
  }

  resetLayoutSelectFlag(){
    if(self.state.layoutObjData.length > 0) {
      self.state.layoutObjData.map(function (categoryObj, catIndex) {
        categoryObj.objRow.map(function(rows, rowIndex){
          rows.objSeat.map(function(cols, colIndex){
            cols.isSelected=false;
          })
        })
      });
    }
    self.setState({
      "layoutObjData":self.state.layoutObjData
    });
  }
  /*Select deselect seat*/
  selectDeselectSeat(selectedObj, category, originalRowIndex, originalColumnIndex, e) {
    if (selectedObj.SeatStatus == "0") {
      if (self.state.layoutObjData.length > 0) {
        self.state.layoutObjData.map(function (categoryObj, catIndex) {
          if (categoryObj.AreaDesc == category) {
            var rowData = categoryObj.objRow[originalRowIndex];
            var colObj = rowData.objSeat[originalColumnIndex];
            colObj.isSelected = colObj.isSelected ? false : true;
            colObj['PhyRowId']=rowData.PhyRowId;
            colObj['categoryInfo']=category;

            /*Here we are updating selected item array*/

            if((self.state.selectedSeats.length>0 && self.state.selectedSeats[0].categoryInfo !== category )||self.state.selectedSeats.length>9){
              self.state.selectedSeats=[];
              self.resetLayoutSelectFlag();
              colObj.isSelected=true;
              self.state.selectedSeats.push(colObj);
              self.setState({
                selectedSeats:self.state.selectedSeats,
                itemCategory:self.state.selectedSeats[0] && self.state.selectedSeats[0].categoryInfo
              });
              return;
            }
            if(colObj.isSelected){
              self.state.selectedSeats.push(colObj);
            }else{
              var duplicateIndex =self.findDuplicateObjIndex(self.state.selectedSeats,colObj,'GridSeatNum','PhyRowId');
              self.state.selectedSeats.splice(duplicateIndex,1);
            }
            self.setState({
              selectedSeats:self.state.selectedSeats,
              itemCategory:self.state.selectedSeats[0] && self.state.selectedSeats[0].categoryInfo
            });

          }
        });
        self.setState({layoutObjData: self.state.layoutObjData});
      }
    }
  }

  /* Apply CSS based on seat status*/
  applySeatStatus(status) {
    var styleValue;
    switch (parseInt(status)) {
      case 0:
        styleValue = 'seatAvailable';
        break;
      case 1:
        styleValue = 'taken';
        break;
      case 3:
        styleValue = 'notAvailbale'
        break;
      default:
        styleValue = "";
    }
    return styleValue;
  }

  /* This mathod is to generate HTML*/
  getSeatLayoutHTML(catgory) {
    let rowData = _.get(catgory, 'objRow', []);
    return (<div>
      {rowData.length > 0 && rowData.map(function (rows, rowIndex) {
        return (<div>
          <ul className="labelContainer">
            <li>
              <span className="label">{rows.PhyRowId}</span>
            </li>
          </ul>
          {rows.objSeat.length > 0 && rows.objSeat.map(function (cols, colIndex) {
            var customStyle = self.applySeatStatus(cols.SeatStatus);
            var customSpacing;
            if (colIndex == 0) {
              customSpacing = {"marginLeft": self.state && self.state.leftspacing ? self.state.leftspacing : 0};
            } else {
              customSpacing = {};
            }
            return (cols.seatNumber && <ul className="seatLabelContainer" style={customSpacing}>
              <li className={"seatStyleContainer " + customStyle + " " + (cols.isSelected ? "active" : "")}
                  onClick={self.selectDeselectSeat.bind(null, cols, catgory.AreaDesc, rowIndex, colIndex)}>
                <span className="seatStyle">{cols.seatNumber}</span>
              </li>
            </ul> || <ul className="seatLabelContainer">
              <li className="seatStyleContainerBlank">
                <span className="seatStyle">&nbsp;</span>
              </li>
            </ul>)
          })}
        </div>)
      })}
    </div>);
  }
  proceedToBook(){
    self.props.seatSelectedFromLayout(self.state.selectedSeats);
    var layoutReducerData = {
        seatLayout:{
          colAreas:{
            objArea:self.state.layoutObjData
          }
        }
    }
    var FinalDataToStore = Object.assign({}, layoutReducerData);
    //console.log(FinalDataToStore);
    self.props.updateLayout(FinalDataToStore);
    self.makeSheetsInCenter(2);
    // self.setState({
    //   selectedSeats:[]
    // });
  }

  componentWillMount() {
    self.getLayoutData();
    self.setState({
      selectedSeats:[]
    });
  }

  componentWillReceiveProps(newProps, oldProps) {
    let seatLayoutArray = _.get(newProps, 'layoutReducer.seatLayout.colAreas.objArea', []);
    this.setState({
      "layoutObjData": seatLayoutArray
    }, function () {
      if(self.state.selectedSeats.length==0){
        this.buildSeatLayoutRows();
      }
    });
  }

  makeSheetsInCenter(n) {
    setTimeout(function () {
      var leftspacing = $(document).width() - $(".listAllSeats").width();
      self && self.setState({"leftspacing": leftspacing / n})
    }, 100)
  }

  componentDidMount() {
    self.makeSheetsInCenter(3);
    window.onresize = function (event) {
      self.makeSheetsInCenter(2);
    }
  }

  render() {
    const {getSeatLayoutHTML} = this;
    let seatLayoutArray = self.state && self.state.layoutObjData ? self.state.layoutObjData : [];
    let enableButton = self.state.selectedSeats.length>0? true:false;
    return (<div>
      <div className="navigationRow">
        <div className="colLeft"><span className="tabs arrow"></span>
          <div><span className="tabs mainHeader">Dear Zindagi</span><br/><span className="tabs subHeader">Hindi | Today, 28 Nov, 02:40 PM</span>
          </div>
          <div className="middleContainer"><span className="tabs middleText">Tickets: </span><span
            className="ticketHeader">{self.state.selectedSeats.length>0?self.state.selectedSeats.length:0}</span>
          </div>
          <div className="middleContainer"><span className="tabs middleText">Category: </span><span
            className="ticketHeader">{self.state.itemCategory?_.capitalize(self.state.itemCategory):""}</span>
          </div>
        </div>
        <div className="colRight"><span className="tabs actionLink proceedToBook"><button className={!enableButton?'disabled':""} onClick={self.proceedToBook}>Proceed to Book</button></span>
        </div>
      </div>
      <div className="layoutContainer">
        {/*<div className="screen screenTop"></div>*/}
        {_.map(seatLayoutArray, (category, index)=> {
          return (<div key={index}>
            <div className="layoutCategory">
              {category.AreaDesc.toUpperCase()}
            </div>
            <div className="listAllSeats">
              {getSeatLayoutHTML(category)}
            </div>
          </div>)
        })}
        <div className="screen screenBottom">Screen this way</div>
        {/*End Layout From Here*/}
        <div className="labelDescription">
          <span className="labelIndicator availableSeatIndicator"></span><span className="labelDescriptionContainer">Available</span>
          <span className="labelIndicator availableSeatIndicator taken"></span><span
          className="labelDescriptionContainer">Taken</span>
          <span className="labelIndicator availableSeatIndicator active"></span><span
          className="labelDescriptionContainer">Selected</span>
          <span className="labelIndicator availableSeatIndicator notAvailbale"></span><span
          className="labelDescriptionContainer">Not Available</span>
        </div>

      </div>
    </div>)
  }
}
