// @flow
import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './containers/App';
import Home from './components/Home/Home';
import ManagePage from './containers/ManagePage';
import LayoutPage from './containers/LayoutPage';


export default (
  <Route path="/" component={App}>
    <IndexRoute component={Home} />
    <Route path="/manage" component={ManagePage}/>
    <Route path="/layout" component={LayoutPage}/>
  </Route>
);
