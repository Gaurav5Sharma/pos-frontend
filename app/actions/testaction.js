import fetch from 'fetch';
import {GOTAPIDATA, UPDATEAPIDATA} from '../utils/constants'
import {mockAPIUrl} from '../utils/urls'

export function getAPIData() {
  return (dispatch: Function) => {
    const resp = fetch.fetchUrl(mockAPIUrl, function(error, meta, body){
      dispatch({
        type: GOTAPIDATA,
        payload: JSON.parse(body.toString())
      })
    });
  };
}

export function updateData() {
  return (dispatch: Function) => {
    dispatch({
      type: UPDATEAPIDATA,
      payload: {postId: 'rdytui', id: 12, email: 'dsfghj', name: 'ftyg', body: 'sadfghjk'}
    })
  };
}
