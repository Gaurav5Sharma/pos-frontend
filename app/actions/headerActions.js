import fetch from 'fetch';
import {CHANGETAB} from '../utils/constants'

export function switchTab(tab) {
  return (dispatch: Function) => {
    dispatch({
      type: CHANGETAB,
      payload: tab
    })
  };
}
