import fetch from 'fetch';
import {GETSEATDATA, SELECTEDSEATS, UPDATELAYOUT} from '../utils/constants'
import {dummySEATJSON} from '../utils/urls'
import seatLayoutData from '../components/Layout/SeatLayout2.json'


export function getSeatData() {
  return (dispatch: Function) => {
      dispatch({
        type: GETSEATDATA,
        payload: seatLayoutData
      })
  };
}

export function updateLayout(dataFromComponent) {
  return (dispatch: Function) => {
    dispatch({
      type: UPDATELAYOUT,
      payload: dataFromComponent
    })
  };
}

export function seatSelectedFromLayout(seatsCollection) {
  return (dispatch: Function) => {
    dispatch({
      type: SELECTEDSEATS,
      payload: seatsCollection
    })
  };
}

