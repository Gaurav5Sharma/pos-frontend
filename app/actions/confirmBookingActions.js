import fetch from 'fetch';
import {CONFIRMBOOKING, TICKETDATA} from '../utils/constants'

export function updateBooking(bookingData) {
  return (dispatch: Function) => {
    dispatch({
      type: CONFIRMBOOKING,
      payload: bookingData
    })
  };
}

export function getTicketData(ticketData) {
  return (dispatch: Function) => {
    dispatch({
      type: TICKETDATA,
      payload: ticketData
    })
  };
}