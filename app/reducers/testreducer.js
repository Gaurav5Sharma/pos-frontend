// @flow
import {GOTAPIDATA, UPDATEAPIDATA} from '../utils/constants'

export default function testreducer(state = [], action = {}) {
  switch (action.type) {
    case GOTAPIDATA:
      return action.payload;
    case UPDATEAPIDATA:
      return [...state, ...[action.payload]];
    default:
      return state;
  }
}
