// @flow
import {GETSEATDATA,SELECTEDSEATS,UPDATELAYOUT} from '../utils/constants'

export  function layoutReducer(state =[], action = {}) {
  switch (action.type) {
    case GETSEATDATA:
      return action.payload;
    case UPDATELAYOUT:
      return action.payload;
    default:
      return state;
  }
}
export  function selectedValue(state =[], action = {}) {
  switch (action.type) {
    case SELECTEDSEATS:
      return action.payload;
    default:
      return state;
  }
}
