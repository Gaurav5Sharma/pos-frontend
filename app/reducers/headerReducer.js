// @flow
import {CHANGETAB} from '../utils/constants'

export function currentTab(state = 'BookTickets', action = {}) {
  switch (action.type) {
    case CHANGETAB:
      return action.payload;
    default:
      return state;
  }
}
