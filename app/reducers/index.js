// @flow
import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import testreducer from './testreducer';
import {layoutReducer,selectedValue} from './layoutReducer';
import { currentTab } from './headerReducer';
import { confirmBooking } from './confirmBookingReducer';

const rootReducer = combineReducers({
  routing,
  testreducer,
  layoutReducer,
  selectedValue,
  currentTab,
  confirmBooking
});

export default rootReducer;
