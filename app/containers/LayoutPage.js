// @flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Layout from '../components/Layout/Layout';
import * as layoutAction from '../actions/layoutAction';

function mapStateToProps(state) {
  return {layoutReducer: state.layoutReducer, selectedValue:state.selectedValue};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(layoutAction, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
