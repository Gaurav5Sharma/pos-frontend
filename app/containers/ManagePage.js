// @flow
import React, { Component } from 'react';
import Manage from '../components/Manage/Manage';

export default class ManagePage extends Component {
  render() {
    return (
      <Manage />
    );
  }
}
