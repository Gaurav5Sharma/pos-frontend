// @flow
import React, { Component, PropTypes } from 'react';
import Header from '../components/Header/Header';
export default class App extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired
  };

  render() {
    return (
      <div>
        <Header />
        {this.props.children}
      </div>
    );
  }
}
