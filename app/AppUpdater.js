import {app, BrowserWindow as BrowserWindowElectron} from "electron";
import * as os from "os";
import {autoUpdater} from "electron-auto-updater";
//import BrowserWindow = GitHubElectron.BrowserWindow

const UPDATE_SERVER_HOST = "http://10.0.134.128:9999/download/latest";

export default class AppUpdater {
  constructor(window: BrowserWindow) {
    if ( app.getPath("exe").includes("/node_modules/electron-prebuilt/")) {
      return
    }

    const platform = os.platform()
    if (platform === "linux") {
      return
    }

    const version = app.getVersion()
    autoUpdater.addListener("update-available", (event: any) => {
      console.log("A new update is available")
    })
    autoUpdater.addListener("update-downloaded", (event: any, releaseNotes: string, releaseName: string, releaseDate: string, updateURL: string) => {
      notify("A new update is ready to install", `Version ${releaseName} is downloaded and will be automatically installed on Quit`)
      console.log("quitAndInstall")
      autoUpdater.quitAndInstall()
      return true

    })
    autoUpdater.addListener("error", (error: any) => {
      console.log(error)
    })
    autoUpdater.addListener("checking-for-update", (event: any) => {
      console.log("checking-for-update")
    })
    autoUpdater.addListener("update-not-available", () => {
      console.log("update-not-available")
    })

    if (platform === "darwin") {
      autoUpdater.setFeedURL(`https://${UPDATE_SERVER_HOST}/update/${platform}_${os.arch()}/${version}`)
    }

    window.webContents.once("did-frame-finish-load", (event: any) => {
      autoUpdater.checkForUpdates()
    })
  }
}

function notify(title: string, message: string) {
  let windows = BrowserWindowElectron.getAllWindows()
  if (windows.length == 0) {
    return
  }

  windows[0].webContents.send("notify", title, message)
}
